# Pingo-frontend
Provide a public interface to interact with pingo api.

## Development
Following technologies are used:
- React / Redux
- Typescript
- Parcel-bundler

## Configuration
All settings are in `src/config.ts`. Some settings can be set using the `.env` file at the package root. All `.env` variables are injected in `process.env.VAR_NAME` by parcel-bundler.
Some default values are already provided in `config.ts`, so `.env` file can be empty for those variables.

## Develop
You can use this package as standalone, running `yarn dev` which will provide you environment config load + live reload.
You can also use this project inside a container, see `pingo` reame for docker-compose configuration.

## Build
Command `yarn build` will compile a build project in `dist` folder.
You just need to serve this with http server, like nginx for example.

## Roadmap
- [x] Set basic boilerplate with linter
- [x] Show all non deleted websites
- [x] minimal ux
- [x] Add a new website
- [x] Update / Delete a website
- [ ] Solve all TODOs
