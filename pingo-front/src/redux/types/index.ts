// Utility typings for redux-thunk
// Transmit action values throw a mapDispatchToProps
import { Action, AnyAction } from 'redux'

// Thunk action made simple
type ActionThunk<T> = (dispatch: any) => T

// Thunk dispatch made simple
export interface DispatchThunk <A extends Action = AnyAction> {
  <T extends A>(action: T): T
  <R>(asyncAction: ActionThunk<R>): R
}
