import { WebsiteGetDTO } from '../../model'

/**
 * All listed websites in websites list
 */
export interface WebsitesState {
  list: WebsiteGetDTO[]
}

export const websitesInitialState: WebsitesState = {
  list: []
}
