import { STORE_WEBSITES, Actions } from './actions'
import { websitesInitialState, WebsitesState } from './state'

export function websitesReducer (state: WebsitesState = websitesInitialState, action: Actions): WebsitesState {
  switch (action.type) {
    case STORE_WEBSITES:
      return Object.assign({}, state, {
        list: [...action.websites]
      })

    default:
      return state
  }
}
