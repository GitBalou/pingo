import axios from '../../plugins/axios'
import { WebsiteGetDTO } from '../../model'

/**
 * GET request for all non deleted websites
 */
export function getWebsites () {
  return (dispatch: Function) => axios.get('websites')
    .then(resp => dispatch(storeWebsites(resp.data)))
}

/**
 * SYNC actions
 */

export const STORE_WEBSITES = 'STORE_WEBSITES'
/**
 * Set stored websites list
 */
export function storeWebsites (websites: WebsiteGetDTO[]) {
  return { type: STORE_WEBSITES as typeof STORE_WEBSITES, websites }
}

export type Actions = ReturnType<typeof storeWebsites>
