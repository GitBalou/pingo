import { websitesReducer, WebsitesState, websitesInitialState } from '../websites'
import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { WebsiteState, websiteInitialState, websiteReducer } from '../website'

export interface AppState {
  websites: WebsitesState,
  website: WebsiteState
}

export const initialState: AppState = {
  websites: websitesInitialState,
  website: websiteInitialState
}

const rootReducer = combineReducers({ websites: websitesReducer, website: websiteReducer })

export const store = createStore(rootReducer, initialState, applyMiddleware(thunk))
