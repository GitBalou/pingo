import { WebsiteGetDTO } from '../../model'

/**
 * Selected website when updating, viewing details, etc...
 */
export interface WebsiteState {
  website: WebsiteGetDTO | null
}

export const websiteInitialState: WebsiteState = {
  website: null
}
