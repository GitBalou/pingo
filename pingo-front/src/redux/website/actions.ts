import axios from '../../plugins/axios'
import { WebsiteGetDTO, WebsitePostDTO } from '../../model'
import { ThunkAction } from 'redux-thunk'

/**
 * ASYNC actions
 * We return a promise and handle following action in component as explained here
 * https://github.com/reduxjs/redux/issues/791
 */

export const GET_WEBSITE = 'GET_WEBSITE'
/**
 * GET a website by id from api
 */
// TODO: validate incoming api data
export function getWebsite (id: string) {
  return (dispatch: Function) => axios.get('websites/' + id)
    .then(resp => dispatch(setSelected(resp.data)))
}

export const POST_WEBSITE = 'POST_WEBSITE'
/**
 * POST a new website
 */
export function postWebsite (dto: WebsitePostDTO) {
  return () => axios.post('websites/', dto)
}

export const PUT_WEBSITE = 'PUT_WEBSITE'
/**
 * PUT request to a website
 */
export function putWebsite (id: string, dto: WebsitePostDTO) {
  return () => axios.put('websites/' + id, dto)
}

export const DELETE_WEBSITE = 'DELETE_WEBSITE'
/**
 * DELETE request for website
 */
export function deleteWebsite (id: string) {
  return () => axios.delete('websites/' + id)
}

export const SET_SELECTED_WEBSITE = 'SET_SELECTED_WEBSITE'

/**
 * SYNC actions
 */

/**
 * Set a website as selected website
 */
export function setSelected (website: WebsiteGetDTO) {
  return { type: SET_SELECTED_WEBSITE as typeof SET_SELECTED_WEBSITE, website }
}

export const CLEAR_WEBSITE = 'CLEAR_WEBSITE'
/**
 * Set selected website to null
 */
export function clearWebsite () {
  return { type: CLEAR_WEBSITE as typeof CLEAR_WEBSITE }
}

export type Actions = ReturnType<typeof setSelected> | ReturnType<typeof clearWebsite>
