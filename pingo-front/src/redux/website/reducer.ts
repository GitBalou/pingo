import { Actions, SET_SELECTED_WEBSITE, CLEAR_WEBSITE } from './actions'
import { websiteInitialState, WebsiteState } from './state'

export function websiteReducer (state: WebsiteState = websiteInitialState, action: Actions): WebsiteState {
  switch (action.type) {
    case SET_SELECTED_WEBSITE:
      return Object.assign({}, state, {
        website: action.website
      })

    case CLEAR_WEBSITE:
      return Object.assign({}, state, {
        website: null
      })

    default:
      return state
  }
}
