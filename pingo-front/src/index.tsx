import * as React from 'react'
import { render } from 'react-dom'
import { store } from './redux/store'
import { Provider } from 'react-redux'
import { AppRouter } from './router'
import * as moment from 'moment'

// moment i18n
import 'moment/locale/fr'
moment.locale('fr')

// Bootstrap
import 'bootstrap/dist/css/bootstrap.css'

const App = () => {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  )
}

render(<App />, document.getElementById('root'))
