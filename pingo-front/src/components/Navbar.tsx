import * as React from 'react'
import NavbarBtrsp from 'reactstrap/lib/Navbar'
import NavbarBrand from 'reactstrap/lib/NavbarBrand'
import NavbarToggler from 'reactstrap/lib/NavbarToggler'
import Collapse from 'reactstrap/lib/Collapse'
import NavItem from 'reactstrap/lib/NavItem'
import Nav from 'reactstrap/lib/Nav'
import { Link } from 'react-router-dom'

interface Prop {}

interface State {
  showCollapsedMenu: boolean
}

export class Navbar extends React.Component<Prop,State> {

  state = {
    showCollapsedMenu: false
  }

  toggleCollapsedMenu = () => {
    this.setState({ showCollapsedMenu: !this.state.showCollapsedMenu })
  }

  render () {
    return (
      <NavbarBtrsp color ='success' dark expand='sm'>

        <NavbarBrand>
          <Link to='/'>Pingo</Link>
        </NavbarBrand>

        <NavbarToggler onClick={this.toggleCollapsedMenu} />

        <Collapse isOpen={this.state.showCollapsedMenu} navbar>
          <Nav navbar className='ml-auto'>
            <NavItem>
              <Link className='nav-link' to='/'>Root</Link>
            </NavItem>
            <NavItem>
              <Link className='nav-link' to='/websites/new'>New</Link>
            </NavItem>
          </Nav>
        </Collapse>

      </NavbarBtrsp >
    )
  }
}
