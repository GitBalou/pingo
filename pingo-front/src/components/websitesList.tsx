import * as React from 'react'
import { WebsiteGetDTO } from '../model'
import ListGroup from 'reactstrap/lib/ListGroup'
import { WebsiteListItem } from './websiteListItem'

interface Props {
  websites: WebsiteGetDTO[]
}

export class WebsitesList extends React.Component<Props> {
  render () {
    return (
      <ListGroup>
        {this.props.websites.map(website => <WebsiteListItem website={website} key={website.id}/>)}
      </ListGroup>
    )
  }
}
