import * as React from 'react'
import { FieldRenderProps, FieldProps } from 'react-final-form'
import Input from 'reactstrap/lib/Input'
import FormFeedback from 'reactstrap/lib/FormFeedback'
import FormGroup from 'reactstrap/lib/FormGroup'

interface State {
  value: string
}

// Props come aggregate final form props + id and label
type Props = FieldProps

const InputTxt = ({ input, meta, id, label }: Props) => (
  <FormGroup>
    <label htmlFor={id}>{label}</label>

    <Input
      id={id}
      type='text'
      {...input}
      invalid={meta.visited && !meta.valid}
    />

    <FormFeedback>
      {meta.error}
    </FormFeedback>
  </FormGroup>
)

// HOC as adapter for react final form. props:
export const InputTxtHandler = (props: FieldProps) => <InputTxt {...props} />
