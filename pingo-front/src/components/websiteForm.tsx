import * as React from 'react'
import { Form as FormFF, Field as FieldFF } from 'react-final-form'
import { InputTxtHandler } from './inputTxtHandler'
import * as validate from 'validate.js'
import { WebsiteGetDTO, WebsitePostDTO } from '../model'
import Form from 'reactstrap/lib/Form'
import Button from 'reactstrap/lib/Button'

/**
 * Set initial form values
 */
const initialFieldsValues = (website: WebsiteGetDTO | null) => {
  return website !== null
    ? website
    : {
      name: '',
      url: ''
    }
}

/**
 * Input validators
 */
const name = (val: string): string | undefined => validate.single(val, {
  presence: { message: 'Champ obligatoire' },
  length: {
    minimum: 2,
    tooShort: 'Trop court. %{count} caractères minimum',
    maximum: 255,
    tooLong: 'Trop long. %{count} caractères maximum'
  },
  format: {
    pattern: /^[a-zA-Z0-9]+$/,
    message: 'Caractères alpha numérique uniquement'
  }
})

const url = (val: string): string | undefined => validate.single(val, {
  presence: { message: 'Champ obligatoire' },
  url: { message: 'URL non valide' }
})

interface Props {
  initialValue: WebsiteGetDTO | null
  onSubmit: (website: WebsitePostDTO) => void
}

/**
 * Form to add or update a website
 */
export const WebsiteForm = ({ initialValue, onSubmit }: Props) => (
  <FormFF
    onSubmit={(values: any) => onSubmit(values)}
    initialValues={initialFieldsValues(initialValue)}
    render={({ handleSubmit, pristine, invalid }) => (
      <Form onSubmit={handleSubmit}>

        <FieldFF
          id='name'
          name='name'
          label='Nom'
          children={InputTxtHandler}
          validate={name}
        />

        <FieldFF
          id='url'
          name='url'
          label='URL'
          children={InputTxtHandler}
          validate={url}
        />

        <div className='clearfix'>
          <Button
            type='submit'
            className='float-right'
            color='accent'
            disabled={pristine || invalid}
          >
            Enregistrer
          </Button>
        </div>
      </Form>
    )}
  />
)
