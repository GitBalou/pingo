import * as React from 'react'
import ListGroupItem from 'reactstrap/lib/ListGroupItem'
import ListGroupItemHeading from 'reactstrap/lib/ListGroupItemHeading'
import ListGroupItemText from 'reactstrap/lib/ListGroupItemText'
import { WebsiteGetDTO } from '../model'
import * as moment from 'moment'
import { Link } from 'react-router-dom'

/**
 * Human readable text for elapsed time since last ping
 */
function lastPingDateForHumans (lastPingAt: Date | null) {
  return lastPingAt
  ? moment(lastPingAt).from(moment())
  : 'Jamais'
}

/**
 * Human readable text for last website status (up or down)
 */
function statusForHumans (upOnLastPing: boolean) {
  return upOnLastPing ? 'UP' : 'DOWN'
}

interface Props {
  website: WebsiteGetDTO
}

export const WebsiteListItem = ({ website }: Props) => (
  <>
    <ListGroupItem action className='link-inside'>
      <Link to={'websites/' + website.id}>
        <div>
          <ListGroupItemHeading>
            {website.name}
          </ListGroupItemHeading>

          <ListGroupItemText>
            {lastPingDateForHumans(website.lastPingAt)}
          </ListGroupItemText>

          <ListGroupItemText>
            Dernier statut connu: {statusForHumans(website.upOnLastPing)}
          </ListGroupItemText>
        </div>
      </Link>
    </ListGroupItem>

    <ListGroupItem>
      <a href={website.url}>{website.url}</a>
    </ListGroupItem>
  </>
)
