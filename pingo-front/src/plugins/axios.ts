import axiosLib from 'axios'
import { config } from '../config'

// TODO: add a generic error handling (at least 50*)
const axios = axiosLib.create({
  baseURL: config.api.url
})

export default axios
