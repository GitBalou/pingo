import * as React from 'react'
import { connect } from 'react-redux'
import { AppState } from '../redux/store'
import { getWebsites } from '../redux/websites'
import { WebsitesList } from '../components/websitesList'

function mapStateToProps (state: AppState) {
  return state.websites
}

function mapDispatchToProps (dispatch: Function) {
  return {
    getWebsites: () => dispatch(getWebsites())
  }
}

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

class WebsitesListToConnect extends React.Component<Props> {
  componentDidMount () {
    this.props.getWebsites()
  }

  render () {
    return (
      <div>
        <WebsitesList websites={this.props.list} />
      </div>
    )
  }
}

export const WebsitesListContainer = connect(mapStateToProps, mapDispatchToProps)(WebsitesListToConnect)
