import * as React from 'react'
import { connect } from 'react-redux'
import { getWebsite, clearWebsite, postWebsite, putWebsite, deleteWebsite } from '../redux/website'
import { AppState } from '../redux/store'
import { WebsiteForm } from '../components/websiteForm'
import { WebsitePostDTO } from '../model'
import Button from 'reactstrap/lib/Button'
import { RouteComponentProps, RouteProps } from 'react-router'
import { DispatchThunk } from '../redux/types'

/**
 * Props handles redux & routing
 */
type RouterProps = RouteComponentProps<{ id: string }>

function mapStateToProps (state: AppState) {
  return state.website
}

function mapDispatchToProps (dispatch: DispatchThunk) {
  return {
    getWebsite: (id: string) => dispatch(getWebsite(id)),
    postWebsite: (dto: WebsitePostDTO) => dispatch(postWebsite(dto)),
    putWebsite: (id: string, dto: WebsitePostDTO) => dispatch(putWebsite(id, dto)),
    deleteWebsite: (id: string) => dispatch(deleteWebsite(id)),
    clearWebsite: () => dispatch(clearWebsite())
  }
}

type Props = RouterProps & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

/**
 * Form to add or update a website
 * Refresh stored website when component is mounted, or when routing changes
 */
class WebsiteFormToConnect extends React.Component<Props> {
  componentDidMount () {
    this.refreshStoredWebsite(this.props.match.params.id)
  }

  componentWillReceiveProps (nextProps: Props) {
    if (nextProps.match.params.id === this.props.match.params.id) {
      return
    }

    this.refreshStoredWebsite(nextProps.match.params.id)
  }

  /**
   * refresh stored website:
   * load website by id if an id is provided,
   * clear the selected website otherwise
   */
  refreshStoredWebsite = (id: string) => {
    if (id) {
      this.props.getWebsite(id)
    } else {
      this.props.clearWebsite()
    }
  }

  /**
   * Create or update website
   */
  onSubmit = async (values: any) => {
    const dto = {
      name:                                                                                                                                            values.name,
      url: values.url
    }

    try {
      if (this.props.website != null) {
        await this.props.putWebsite(this.props.website.id, dto)
      } else {
        await this.props.postWebsite(dto)
      }

      this.props.history.push('/')
    } catch (e) {
      // TODO: error handling
      console.log('err', e)
    }

  }

  /**
   * Delete a website using it's id
   */
  delete = async () => {
    if (this.props.website === null) {
      return
    }

    try {
      await this.props.deleteWebsite(this.props.website.id)
      this.props.history.push('/')
    } catch (e) {
      console.log(e)
    }
  }

  render () {
    return (
      <div>
        <WebsiteForm
          initialValue={this.props.website}
          onSubmit={this.onSubmit}
        />

        {
          this.props.website !== null &&
          <div
            className='mt-4 pt-2 border-top d-flex justify-content-between align-items-center'>
            Supprimer le site ?
            <Button color='danger' onClick={this.delete}>Supprimer</Button>
          </div>
        }
      </div>
    )
  }
}

export const WebsiteFormContainer = connect(mapStateToProps, mapDispatchToProps)(WebsiteFormToConnect)
