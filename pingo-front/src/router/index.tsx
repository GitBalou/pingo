import * as React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import { WebsitesListContainer } from '../containers/websitesListContainer'
import { WebsiteFormContainer } from '../containers/websiteFormContainer'
import { DefaultLayout } from './default'

import 'bootstrap/dist/css/bootstrap.min.css'

export const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <DefaultLayout exact path='/websites/new' component={WebsiteFormContainer} />
      <DefaultLayout exact path='/websites/:id' component={WebsiteFormContainer} />
      <DefaultLayout exact path='/websites' component={WebsitesListContainer} />
      <DefaultLayout exact path='/' component={WebsitesListContainer} />
    </Switch>
  </BrowserRouter>
)
