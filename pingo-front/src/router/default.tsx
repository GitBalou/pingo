import * as React from 'react'
import { Route, RouteProps } from 'react-router-dom'
import { Navbar } from '../components/Navbar'

/**
 * Default app layout
 * Wraps the given react component between a header and a footer
 */
export const DefaultLayout = ({ component: Component, ...rest }: { component: React.ComponentType<any>} & RouteProps) => {
  return (
    <Route {...rest} render={matchProps => (
      <div className='DefaultLayout'>

        <Navbar></Navbar>

        <div className='container mt-4 mb-4'>
          <Component {...matchProps} />
        </div>

        <div className='border-top text-center'>
          Pingo - MVP
        </div>
      </div>
    )} />
  )
}
