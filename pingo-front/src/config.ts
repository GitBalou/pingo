export const config = {
  api: {
    url: process.env.API_URL || 'http://localhost:3000'
  }
}
