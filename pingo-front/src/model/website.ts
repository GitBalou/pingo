// TODO: validate this model when coming from api
/**
 * Data descrinbing a website
 */
export interface WebsiteGetDTO {
  id: string
  name: string
  url: string
  lastPingAt: Date
  upOnLastPing: boolean
}

/**
 * Data for website creation
 */
export interface WebsitePostDTO {
  name: string
  url: string
}
