package model

import (
	"time"
)

// WebsiteRAM structure for RAM storage
type WebsiteRAM struct {
	ID           string     `json:"id"`
	Name         string     `json:"name"`
	URL          string     `json:"url"`
	LastPingAt   *time.Time `json:"lastPingAt"`
	UpOnLastPing *bool      `json:"upOnLastPing"`
	CreatedAt    *time.Time `json:"createdAt"`
	UpdatedAt    *time.Time `json:"updatedAt"`
	DeletedAt    *time.Time `json:"deletedAt"`
}

// WebsiteDTO creation DTO
type WebsiteDTO struct {
	Name string `json:"name" valid:"required, alphanum"`
	URL  string `json:"url" valid:"required, url"`
}
