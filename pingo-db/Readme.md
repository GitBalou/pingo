# Pingo-db

Postgresql database for pingo.

## Init the db
Create a new database, for exemple `pingo` with owner `pingo`.

Use the dump `init.sql` to create db schema.

## TODO
- [ ] make the container init's it's db with a provided dump
- [ ] duplicate this container to have a test container, to test api