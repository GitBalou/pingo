package psql

import (
	"database/sql"
	"log"
)

// Db share db pointer accross all submodules. Call it using psql.Db
var Db *sql.DB

// MustOpen open a connection with a postgresql database and test credentials with a ping
func MustOpen(dbuser string, dbpassword string, dbhost string, dbname string) *sql.DB {
	// outside docker
	// connectionStr := "user=pingo dbname=pingo password=password port=5433 sslmode=disable"
	// inside docker
	// connectionStr := "postgres://pingo:password@postgres/pingo?sslmode=disable"

	connectionStr := "postgres://"
	connectionStr += dbuser + ":" + dbpassword
	connectionStr += "@" + dbhost
	connectionStr += "/" + dbname
	connectionStr += "?sslmode=disable"

	db, err := sql.Open("postgres", connectionStr)
	if err != nil {
		log.Print(err)
		log.Print("With credentials", connectionStr)
		panic(err)
	}

	// Check that our connection credentials are valid before starting
	err = db.Ping()
	if err != nil {
		log.Print(err)
		panic(err)
	}

	return db
}

// StoreDb the given database pointer, to make it accessible to all app
func StoreDb(dbref *sql.DB) {
	Db = dbref
}
