# PinGo
Ping websites for monitoring

## Architecture
API - written in Go - Expose a CRUD to add / modify / remove an url to ping
Db - postgresql - store websites to ping and last ping result
Monitoring service - written in Go - Monitors urls stored in db

## Running on localhost
Git clone this project in your $GOPATH sources, you should end with something like `c:/go/src/pingo`

You will need docker & docker-compose as dev dependencies.
Type `docker-compose up` to start all containers.

- Container `pingo-pg` is the posgresql database. You can connect from outside on port `5433`. Dev user is `pingo`, dev database is `pingo`, dev password is `password`. For exemple: `psql -U pingo -p 5433 pingo` will connect you to the database running in the container.

- Container `pingo-api` is our application api, it's listening on port `3000`.
We use `gin` package to provide livereload, so you can dev in your IDE and api will be restarted on each file save.

- Container `pingo-ping` is our monitoring service. It reads what website it has to ping in database, ping them, and update database with status. Livereload is also provided inside docker.

- Container `pingo-front` is a basic frontend powered by parcel-bundler, listening on port `1234`. Livereload is also enabled.

## Naming conventions
DTOs & struct name are pascal-cased. eg: Name, MyField. Except for some fields where go conventions are above (ID, URL)
SQL fields are lowercased, without any kind of separator, and should match corresponding structure fields.
eg: 
Struct{ ID, Name, CreateDate } => sql table { id, name, createdate }

# MVPs
- [x] API expose a basic CRUD, everything is stored in RAM
- [x] Persistent Db
- [x] Task runner
- [x] Framework extraction from API and Ping (check all todo)
- [x] Env variable for conf
- [x] Frontend
- [x] AWS deployment
- [ ] Gitlab & CI/CD
- [ ] Better management security on aws (set db access, group permissions, ...)
- [ ] Add logs for api & ping (maybe aws has something?)
- [ ] Document api (like swagger ?)
- [ ] implements tests
- [ ] Use protobuf for communication between go api & ts fronts