package config

import (
	"os"
)

// Config regroup all program config
type Config struct {
	DBName         string
	DBHost         string
	DBUser         string
	DBPassword     string
	MonitoringCRON string
}

// MustLoad load config using default values & environment values
// It panics if some required values are missing
func MustLoad() Config {
	dbname := os.Getenv("DBNAME")
	if dbname == "" {
		panic("DBNAME is not set in env")
	}

	dbhost := os.Getenv("DBHOST")
	if dbhost == "" {
		panic("DBHOST is not set in env")
	}

	dbuser := os.Getenv("DBUSER")
	if dbuser == "" {
		panic("DBUSER is not set in env")
	}

	dbpassword := os.Getenv("DBPASSWORD")
	if dbpassword == "" {
		panic("DBPASSWORD is not set in env")
	}

	return Config{
		DBName:         dbname,
		DBHost:         dbhost,
		DBUser:         dbuser,
		DBPassword:     dbpassword,
		MonitoringCRON: "0 */10 * * * *",
	}
}
