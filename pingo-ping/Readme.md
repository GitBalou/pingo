# Pingo ping
Service responsible to ping websites stored in db

## How does it work
A cron start a task every 10 minutes.
That task:
- read all non deleted websites
- start a coroutine for each website

In each coroutine:
- ping the website
- save in db it's state (up or down)

## Dev dependencies
We use `gin` package to provide livereload, so you can dev in your IDE and api will be restarted on each file save.
- you can install it using `go get github.com/codegangsta/gin`
- to run the api using gin: `gin -i -p 3001 run main.go`

We use `godotenv` , command line version, to manage our .env file.
- You can install it with `go get github.com/joho/godotenv/cmd/godotenv`
- to run the api using godotenv: `godotenv go run main.go`

Using both gin & godotenv, you can run the api this way: `godotenv gin -i -p 3001 run main.go`