package dao

import (
	"context"
	"pingo/pingo-db"
	"pingo/pingo-shared/model"
)

// WebsitesToPing find all non deleted websites
func WebsitesToPing(ctx context.Context) ([]model.WebsiteRAM, error) {
	websites := make([]model.WebsiteRAM, 0)

	rows, err := psql.Db.QueryContext(ctx, "SELECT * FROM website WHERE deletedat IS NULL")
	if err != nil {
		return websites, err
	}
	defer rows.Close()

	for rows.Next() {
		var website model.WebsiteRAM
		err := rows.Scan(
			&website.ID,
			&website.Name,
			&website.URL,
			&website.LastPingAt,
			&website.UpOnLastPing,
			&website.CreatedAt,
			&website.UpdatedAt,
			&website.DeletedAt,
		)

		if err != nil {
			return websites, err
		}

		websites = append(websites, website)
	}

	return websites, err
}

// WebsiteUpdateStatus update all fields of a website matching where ID
func WebsiteUpdateStatus(ctx context.Context, status bool, where *model.WebsiteRAM) (model.WebsiteRAM, error) {
	var website model.WebsiteRAM

	row := psql.Db.QueryRowContext(
		ctx,
		"UPDATE website SET uponlastping = $1, lastpingat = NOW() WHERE id = $2 RETURNING *",
		status,
		where.ID,
	)

	err := row.Scan(
		&website.ID,
		&website.Name,
		&website.URL,
		&website.LastPingAt,
		&website.UpOnLastPing,
		&website.CreatedAt,
		&website.UpdatedAt,
		&website.DeletedAt,
	)

	return website, err
}
