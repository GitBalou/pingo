package main

import (
	"log"
	"pingo/pingo-db"
	"pingo/pingo-ping/config"
	"pingo/pingo-ping/runner"

	_ "github.com/lib/pq"
)

func main() {
	config := config.MustLoad()

	// Start db connection
	db := psql.MustOpen(config.DBUser, config.DBPassword, config.DBHost, config.DBName)

	// Save db pointer to share it across all modules
	psql.StoreDb(db)

	// Start runner
	runner.Init(config.MonitoringCRON)

	// Track if program exited normally, which it should not
	log.Println("main exited")
}
