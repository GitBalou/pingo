package runner

import (
	"log"

	"github.com/robfig/cron"
)

// Init start all tasks with their given cron
// cronStr should be a valid cron expression, like 0 */1 * * * *
func Init(cronStr string) {
	c := cron.New()

	c.AddFunc(cronStr, MonitorWebsites)

	c.Start()

	log.Println("CRON started")

	// Listen to a chan when they're wont be emission: main function keeps open
	<-make(chan bool)
}
