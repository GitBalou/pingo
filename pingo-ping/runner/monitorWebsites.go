package runner

import (
	"context"
	"log"
	"net/http"
	"pingo/pingo-ping/dao"
	"pingo/pingo-shared/model"
	"sync"
)

// MonitorWebsites pings websites in database
func MonitorWebsites() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Get websites to update
	websites, err := dao.WebsitesToPing(ctx)
	if err != nil {
		log.Printf("Error loading websites %s", err)
		return
	}

	if len(websites) == 0 {
		log.Printf("No website to ping")
		return
	}

	// Sync group to wait for all goroutines
	var wg sync.WaitGroup
	wg.Add(len(websites))

	// Websites are checked in goroutines
	for i := range websites {
		go checkStatus(ctx, &wg, &websites[i])
	}

	// Wait for all goroutines to finish before closing this function,
	// which will cancel the context (defer because defer cancel())
	wg.Wait()
}

// Ping with a GET URL request. We consider website down if an error occured, false otherwise
func checkStatus(ctx context.Context, wg *sync.WaitGroup, website *model.WebsiteRAM) {
	defer wg.Done()

	// TODO: cancel request if context is canceled
	_, err := http.Get(website.URL)
	status := err == nil

	// Update db
	_, err = dao.WebsiteUpdateStatus(ctx, status, website)

	if err != nil {
		log.Println("Error when checking status", err)
	} else {
		log.Printf("Is %s at %s up ? %v", website.Name, website.URL, status)
	}
}
