package handler

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Respond to ping
func ping(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	w.Write([]byte("Api is up"))
}

// Monitoring is used for monitoring api
// All HTTP verbs will receive the same response
func Monitoring(route string, router *httprouter.Router) *httprouter.Router {
	router.GET(route, ping)
	return router
}
