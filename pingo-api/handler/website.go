package handler

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"pingo/pingo-api/dao/websitedao"
	"pingo/pingo-api/framework"
	"pingo/pingo-shared/model"
	"regexp"

	"github.com/asaskevich/govalidator"
	"github.com/julienschmidt/httprouter"
)

// Store website in RAM for now
var storedWebsites []model.WebsiteRAM

// params regexp
var re = regexp.MustCompile("^/websites/([^/ ])$")

// Website handles CRUD for websites
func Website(route string, router *httprouter.Router) *httprouter.Router {
	router.POST(route, create)
	router.GET(route, find)
	router.GET(route+"/:id", getByID)
	router.PUT(route+"/:id", update)
	router.DELETE(route+"/:id", deleteSoft)
	return router
}

// Create a websites, err := website and add it to the store
func create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// Define a new cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Decode body
	var dto model.WebsiteDTO
	err := framework.ParseAndValidateBody(&dto, r)
	if err != nil {
		log.Printf("Error validating body %s", err)
		http.Error(w, "Bad_Request", http.StatusBadRequest)
		return
	}

	website, err := websitedao.Create(ctx, dto)
	if err != nil {
		log.Printf("Error creating website %s", err)
		http.Error(w, "Error creating website", http.StatusInternalServerError)
		return
	}

	jData, err := json.Marshal(website)
	if err != nil {
		http.Error(w, "Error encoding website", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(201)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

// Returns all stored websites
func find(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	// Define a new cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var where websitedao.Filters

	q := r.URL.Query()
	if v := q.Get("name"); v != "" {
		where.Name = v
	}

	websites, err := websitedao.Find(ctx, where)
	if err != nil {
		log.Printf("Error finding websites %s", err)
		http.Error(w, "Error finding websites", http.StatusInternalServerError)
		return
	}

	// Encode to json
	jData, err := json.Marshal(websites)
	if err != nil {
		http.Error(w, "Error encoding website", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

// Returns a website using it's id
func getByID(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Define a new cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Id should be an uuid v4
	id := ps.ByName("id")
	if govalidator.IsUUIDv4(id) == false {
		http.Error(w, "Invalid params", http.StatusBadRequest)
		return
	}

	// Find website ref
	where := websitedao.Filters{ID: id}

	// Query string
	q := r.URL.Query()
	if v := q.Get("name"); v != "" {
		where.Name = v
	}

	website, err := websitedao.FindOne(ctx, where)

	// Not found error & others errors
	if err != nil && website.ID == "" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	} else if err != nil {
		log.Printf("Error finding websites %s", err)
		http.Error(w, "Error finding websites", http.StatusInternalServerError)
		return
	}

	// Encode website
	jData, err := json.Marshal(website)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

// Update a website
func update(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Define a new cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Id should be an uuid v4
	id := ps.ByName("id")
	if govalidator.IsUUIDv4(id) == false {
		http.Error(w, "Invalid params", http.StatusBadRequest)
		return
	}

	// Decode body
	var dto model.WebsiteDTO
	err := framework.ParseAndValidateBody(&dto, r)
	if err != nil {
		log.Printf("Error validating body %s", err)
		http.Error(w, "Bad_Request", http.StatusBadRequest)
		return
	}

	// Update website
	website, err := websitedao.UpdateByID(ctx, dto, id)

	// Not found error & others errors
	if err != nil && website.ID == "" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	} else if err != nil {
		log.Printf("Error updating websites %s", err)
		http.Error(w, "Error updating websites", http.StatusInternalServerError)
		return
	}

	// Encode json
	jData, err := json.Marshal(website)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}

// Soft delete a website
func deleteSoft(w http.ResponseWriter, _ *http.Request, ps httprouter.Params) {
	// Define a new cancelable context
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Id should be an uuid v4
	id := ps.ByName("id")
	if govalidator.IsUUIDv4(id) == false {
		http.Error(w, "Invalid params", http.StatusBadRequest)
		return
	}

	// Delete soft website
	website, err := websitedao.DeleteSoftByID(ctx, id)

	// Not found error & others errors
	if err != nil && website.ID == "" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	} else if err != nil {
		log.Printf("Error deleting websites %s", err)
		http.Error(w, "Error deleting websites", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(204)
}
