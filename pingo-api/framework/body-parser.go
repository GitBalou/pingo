package framework

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/asaskevich/govalidator"
)

// ParseAndValidateBody extract json datas from request body and
// copy it into v. Then it validate v using it's tag leveraging govalidator library
func ParseAndValidateBody(v interface{}, r *http.Request) error {
	// Read body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading body %s", err)
		return err
	}

	// Hydrate
	err = json.Unmarshal(body, v)
	if err != nil {
		log.Printf("Error parsing body %s", err)
		return err
	}

	// Validate
	_, err = govalidator.ValidateStruct(v)
	return err
}
