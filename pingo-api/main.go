package main

import (
	"log"
	"pingo/pingo-api/config"
	"pingo/pingo-api/server"
	"pingo/pingo-db"

	_ "github.com/lib/pq"
)

func main() {
	// Load config
	conf := config.MustLoad()

	// Store config to share it across all modules
	config.StoreConfig(conf)

	// Start db connection
	db := psql.MustOpen(conf.DBUser, conf.DBPassword, conf.DBHost, conf.DBName)

	// Save db pointer to share it across all modules
	psql.StoreDb(db)

	// Start listening
	log.Fatal(server.Serve(conf.Port))
}
