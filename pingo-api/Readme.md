# PINGO API

## Data structure
website:
- id
- name
- url
- last ping at
- up on last ping
- created at
- updated at
- deleted at

## Routes
- `GET /` monitoring
- `GET /websites`
- `GET /websites/:id`
- `POST /websites`
- `UPDATE /websites/:id`
- `DELETE /websites/:id`

## Env variables
Following environment variables are expected:
- API_PORT
- DBNAME
- DBHOST     
- DBUSER     
- DBPASSWORD

You can provide those env variables using docker-compose.yml, setting env externally, or using a dotenv package like https://github.com/joho/godotenv
.env file should not be commited 

## Dev dependencies
We use `gin` package to provide livereload, so you can dev in your IDE and api will be restarted on each file save.
- you can install it using `go get github.com/codegangsta/gin`
- to run the api using gin: `gin -i -p 3001 run main.go`

We use `godotenv` , command line version, to manage our .env file.
- You can install it with `go get github.com/joho/godotenv/cmd/godotenv`
- to run the api using godotenv: `godotenv go run main.go`

Using both gin & godotenv, you can run the api this way: `godotenv gin -i -p 3001 run main.go`