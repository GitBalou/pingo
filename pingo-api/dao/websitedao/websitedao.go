package websitedao

import (
	"context"
	"pingo/pingo-db"
	"pingo/pingo-shared/model"
	"strconv"
	"strings"
)

/**
DAO: still not an abstract class, but there is a pattern here that could be extended
Exported methods that could be common to all DAO:
	- Create
	- Find
	- FindOne
	- UpdateById
	- DeleteSoftById
And it also expose a dedicated filters struct: Filters, used by Find and FindOne
the Filters struct exposes fields the DAO can use for filtering
the filtersToWhereClause internal method create the "WHERE ..." sql clause using the Filters struct
*/

// TODO: See how to make this more abstact or generic if needed ?
// TODO: UpdateById should divided in UpdateOne & Update, using filters
// TODO: DeleteSoftById should also be divided as UpdateById

// whereClauses store prepared sql query WHERE part + corresponding params value
type whereClauses struct {
	Str    string
	Params []interface{}
}

// Filters are available filters when generating the WHERE clause in sql
type Filters struct {
	ID      string
	Name    string
	Deleted bool
}

// filtersToWhereClause create WHERE ... AND ... clauses using the where structure
func filtersToWhereClause(filters *Filters) whereClauses {
	// Initial query is a select *
	selectWhereQuery := whereClauses{
		Str:    " WHERE ",
		Params: make([]interface{}, 0),
	}

	if filters == nil {
		return selectWhereQuery
	}

	var clauses []string // Slice storing sql clauses ['name = $1', 'deletedat IS NOT NULL']
	i := 0

	// Id need an exact match
	if filters.ID != "" {
		selectWhereQuery.Params = append(selectWhereQuery.Params, filters.ID)
		i = len(selectWhereQuery.Params)
		clauses = append(clauses, "id = $"+strconv.Itoa(i))
	}

	// Name do not need an exact match
	if filters.Name != "" {
		selectWhereQuery.Params = append(selectWhereQuery.Params, "%"+filters.Name+"%")
		i = len(selectWhereQuery.Params)
		clauses = append(clauses, "name ILIKE $"+strconv.Itoa(i))
	}

	// deleted is a null match on delete date
	if filters.Deleted == false {
		clauses = append(clauses, "deletedat IS NULL")
	}

	// Add where conditions (it's AND only)
	selectWhereQuery.Str += strings.Join(clauses, " AND ")

	return selectWhereQuery
}

// Create create a new website
// It listen to context to cancel job
func Create(ctx context.Context, dto model.WebsiteDTO) (model.WebsiteRAM, error) {
	var website model.WebsiteRAM

	row := psql.Db.QueryRowContext(
		ctx,
		"INSERT INTO website(name, url) VALUES($1, $2) RETURNING *",
		dto.Name,
		dto.URL,
	)

	err := row.Scan(
		&website.ID,
		&website.Name,
		&website.URL,
		&website.LastPingAt,
		&website.UpOnLastPing,
		&website.CreatedAt,
		&website.UpdatedAt,
		&website.DeletedAt,
	)

	return website, err
}

// Find find all websites matching where parameters
func Find(ctx context.Context, filters Filters) ([]model.WebsiteRAM, error) {
	websites := make([]model.WebsiteRAM, 0)
	query := "SELECT * FROM website"

	// Generate query string with optionnal where conditions
	whereClauses := filtersToWhereClause(&filters)
	query += whereClauses.Str

	rows, err := psql.Db.QueryContext(ctx, query, whereClauses.Params...)

	if err != nil {
		return websites, err
	}
	defer rows.Close()

	for rows.Next() {
		var website model.WebsiteRAM
		err := rows.Scan(
			&website.ID,
			&website.Name,
			&website.URL,
			&website.LastPingAt,
			&website.UpOnLastPing,
			&website.CreatedAt,
			&website.UpdatedAt,
			&website.DeletedAt,
		)

		if err != nil {
			return websites, err
		}

		websites = append(websites, website)
	}

	return websites, err
}

// FindOne find one website matching where parameters
func FindOne(ctx context.Context, filters Filters) (model.WebsiteRAM, error) {
	var website model.WebsiteRAM
	query := "SELECT * FROM website"

	// Generate query string with optionnal where conditions
	whereClauses := filtersToWhereClause(&filters)
	query += whereClauses.Str

	// Only one result expected
	query += " LIMIT 1"

	row := psql.Db.QueryRowContext(ctx, query, whereClauses.Params...)

	err := row.Scan(
		&website.ID,
		&website.Name,
		&website.URL,
		&website.LastPingAt,
		&website.UpOnLastPing,
		&website.CreatedAt,
		&website.UpdatedAt,
		&website.DeletedAt,
	)

	return website, err
}

// UpdateByID update all fields of a website matching where ID
func UpdateByID(ctx context.Context, dto model.WebsiteDTO, id string) (model.WebsiteRAM, error) {
	var website model.WebsiteRAM

	row := psql.Db.QueryRowContext(
		ctx,
		"UPDATE website SET name = $1, url = $2, updatedat = NOW() WHERE id = $3 RETURNING *",
		dto.Name,
		dto.URL,
		id,
	)

	err := row.Scan(
		&website.ID,
		&website.Name,
		&website.URL,
		&website.LastPingAt,
		&website.UpOnLastPing,
		&website.CreatedAt,
		&website.UpdatedAt,
		&website.DeletedAt,
	)

	return website, err
}

// DeleteSoftByID mark a website as deleted using where parameter
func DeleteSoftByID(ctx context.Context, id string) (model.WebsiteRAM, error) {
	var website model.WebsiteRAM

	row := psql.Db.QueryRowContext(
		ctx,
		"UPDATE website SET deletedat = NOW() WHERE id = $1 RETURNING *",
		id,
	)

	err := row.Scan(
		&website.ID,
		&website.Name,
		&website.URL,
		&website.LastPingAt,
		&website.UpOnLastPing,
		&website.CreatedAt,
		&website.UpdatedAt,
		&website.DeletedAt,
	)

	return website, err
}
