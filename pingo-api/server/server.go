package server

import (
	"net/http"
	"pingo/pingo-api/config"
	"pingo/pingo-api/handler"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/cors"
)

// Serve on given port
func Serve(port int) error {
	// Routing
	router := httprouter.New()
	router = handler.Monitoring("/", router)
	router = handler.Website("/websites", router)

	// Middleware common to all routes
	var AllowedHeaders = []string{
		"Range",
		"Accept",
		"Content-Type",
		"Authorization",
		"X-CA-Session",
		"X-Requested-With",
	}

	c := cors.New(cors.Options{
		AllowedOrigins:   config.Conf.CorsURLs,
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   AllowedHeaders,
	}).Handler(router)

	return http.ListenAndServe(":"+strconv.Itoa(port), c)
}
