package config

import (
	"log"
	"os"
	"strconv"
	"strings"
)

// Config regroup all program config
type Config struct {
	Port       int
	CorsURLs   []string
	DBName     string
	DBHost     string
	DBUser     string
	DBPassword string
}

// Conf export a pointer to a saved config, so it's accessible in all app
var Conf *Config

// MustLoad load config using default values & environment values
// It panics if some required values are missing
func MustLoad() *Config {
	port := os.Getenv("API_PORT")
	if port == "" {
		log.Print("API_PORT is not set in env")
		panic("Failed to load config")
	}

	portInt, err := strconv.Atoi(port)
	if err != nil {
		log.Print("API_PORT env is not a valid integer")
		panic("Failed to load config")
	}

	CorsURLsStr := os.Getenv("CORS_URLS")
	if CorsURLsStr == "" {
		log.Print("CORS_URLS is not set in env")
		panic("Failed to load config")
	}

	CorsURLs := strings.Split(CorsURLsStr, ",")

	dbname := os.Getenv("DBNAME")
	if dbname == "" {
		log.Print("DBNAME is not set in env")
		panic("Failed to load config")
	}

	dbhost := os.Getenv("DBHOST")
	if dbhost == "" {
		log.Print("DBHOST is not set in env")
		panic("Failed to load config")
	}

	dbuser := os.Getenv("DBUSER")
	if dbuser == "" {
		log.Print("DBUSER is not set in env")
		panic("Failed to load config")
	}

	dbpassword := os.Getenv("DBPASSWORD")
	if dbpassword == "" {
		log.Print("DBPASSWORD is not set in env")
		panic("Failed to load config")
	}

	return &Config{
		Port:       portInt,
		CorsURLs:   CorsURLs,
		DBName:     dbname,
		DBHost:     dbhost,
		DBUser:     dbuser,
		DBPassword: dbpassword,
	}
}

// StoreConfig store ref to a loaded config
func StoreConfig(ref *Config) {
	Conf = ref
}
