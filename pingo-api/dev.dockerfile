FROM golang:1.10

# Install dev packages
RUN go get github.com/codegangsta/gin
RUN go get github.com/joho/godotenv/cmd/godotenv

# OPEN API PORT
EXPOSE 3000

# Volume containing package files is mounter in docker-compose
# so we have to wait for the container to start to install dependencies
CMD cd /go/src/pingo/pingo-api && go get . && godotenv gin -i -p 3001 run main.go