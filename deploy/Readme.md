# Deploy to AWS
This folder contains scripts & configurations files to automate deploy on AWS - elastic bean service.
Each service (api, ping, frontend, ...) is build as a standalone docker image. Each image as to be pulled on an docker repository (like docker hub, gitlab repository, ...) which has to be accessible from aws servers. `Dockerrun.aws.config` contains aws elastic bean configuration to run corresponding images. 

**Important**: in originals `Dockerrun.aws.config` line `"image": "SETREGISTRY/SETIMAGE",` will hav to be customized in order to make thinks works. It can be done manually or using a script, as explained further.

## Build: platform agnostic
This build procedure will provides you a docker image for each of the service. You can deploy each image wherever you want: a vps, aws, kubernetes ...
Read this part until the end to see how you have to configure all containers to work together.

### Database
You need a postgres server running wherever you want. See the readme.md in `pingo-db` for initalizing / managing this server.

### Frontend
Dockerfile is `front.dockerfile`. Project is build inside the image. You can override some configuration variable at build time using `--build-arg` option.

To build an image, cd in `pingo` and run:
`docker build --build-arg conf_var=my_value -t pingo-front -f ./deploy/front/front.dockerfile .`

To test your image in local:
`docker run -p 8080:80 --name pingo-front-prod pingo-front`
This will provide you a frontend server running at localhost:8080

### Api
Dockerfile is `api.dockerfile`. Project is build inside the image. You can override some configuration variable at build time using `--build-arg` option.

IMPORTANT: API_PORT env var is set at 3000 as default. You can change this using `--build-arg` options, but you will have to update port mapping in `Dockerrun.aws.json` then.

To build an image, cd in `pingo` and run:
`docker build --build-arg conf_var=my_value -t pingo-api -f ./deploy/api/api.dockerfile .`

To test your image in local:
`docker run -p 3000:3000 --name pingo-api-prod pingo-api`
This will provide you an api server running at localhost:3000

### Ping
Dockerfile is `ping.dockerfile`. Project is build inside the image. You can override some configuration variable at build time using `--build-arg` option.

To build an image, cd in `pingo` and run:
`docker build --build-arg conf_var=my_value -t pingo-ping -f ./deploy/ping/ping.dockerfile .`

To test your image in local:
`docker run -p 8081:80 --name pingo-ping-prod pingo-ping`
This will provide you a ping application emitting / listening on port 8081.

### Make the whole things works
Here as some env variables needed so every service will know how to access the others. On some platform like google.cloud, you can configure env value from platform. On others like aws, you have to provide value directly when building docker images.

##### API: DB + FRONT
Api needs to know db credentials (DBNAME, ...) and front url for CORS. See pingo-api/.env file for variables names
Api needs port 3000 & 5432 to be open.

##### FRONT: API
Front needs to know api url. See pingo-front/.env for variables name.
Front needs port 80 to be open.

##### PING: DB
Ping service needs to know db credentials (DBNAME, ...). See pingo-ping/.env file for variables names
Ping needs ports 80 & 5432 to be open.

## Deploy on AWS

### Manually
This is deployment step for aws + elastic beanstalk. You need to know how aws works first...

First build docker images, push them on a docker registry, and update corresponding `Dockerrun.aws.json` to point to relevant registry/image. 

Create an elastic bean application for each service, then create new environment (like dev environment) for each service. Environment has to be configured as Docker multiplatform.
As source code, use corresponding `Dockerrun.aws.json`.

Create an RDS postgres database, and configure security groups so api & ping can access db.

To update an application, create a new image, push it to a docker registry and reload it by uploading `Dockerrun.aws.json` again.

### CI Scripts
Here is a script for gitlab CI (unix). It needs following dependencies:
- aws cli
- zip cli
- sed
A docker image is available with all those dependencies: `gabriellonestone/aws-deploy`. Check it's readme to see how to mount a volume inside and use it.

You'll also need an amazon S3 bucket to store uploaded source code.

Example: building and deploying front.
To run this script, you need to be in `pingo` root folder.
```
# $APP: application name
# $VERSION: version label
# $ENV: environement to update
# DOCKERFILE: path to dockerfile (eg. ./pingo/front/front.dockerfile)
# $REGION: aws region, where you want to deploy
# $EB_BUCKET: S3 bucket storing source code

$AWS_REGISTRY = $AWS_ACCOUNT_ID.dkr.ecr.$REGION.amazonaws.com
$ZIP = sourcecode

# Build container
docker build -t $APP:$VERSION -f $DOCKERFILE . # Don't forget to add real --build-args here

# Authenticate against AWS Docker registry
aws configure set default.region $REGION
eval $(aws ecr get-login)

# Build and push the image
docker build -t $NAME:$VERSION .
docker tag $NAME:$VERSION $AWS_REGISTRY/$NAME:$VERSION
docker push $AWS_REGISTRY/$NAME:$VERSION

# Update Dockerrun.aws.json with corresponding registry/image
sed -i='' "s/myregistry/$AWSREGISTRY/" Dockerrun.aws.json
sed -i='' "s/myimage/$APP:$VERSION/" Dockerrun.aws.json

# Zip source code
zip -r $ZIP Dockerrun.aws.json

# Upload source code to s3
aws s3 cp $ZIP s3://$EB_BUCKET/$ZIP

# Create a new application version with the zipped up Dockerrun file
aws elasticbeanstalk create-application-version --application-name $APP --version-label $VERSION --source-bundle S3Bucket=$EB_BUCKET,S3Key=$ZIP

# Update the environment to use the new application version
aws elasticbeanstalk update-environment --environment-name $ENV --version-label $VERSION
```

