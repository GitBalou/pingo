FROM nginx:mainline-alpine

# Set env from build command: docker build --build-arg var=value ...
ARG API_URL
ENV API_URL=$API_URL

# install yarn for build
RUN apk update && apk add yarn

# copy files & build
WORKDIR /usr/node/app/pingo-front
COPY ./pingo-front .
RUN yarn install
RUN yarn build

# Build is served by nginx
RUN cp -R ./dist/* /usr/share/nginx/html

# Remove useless files
RUN cd .. & rm -Rf ./pingo-front
RUN apk del yarn

EXPOSE 80
