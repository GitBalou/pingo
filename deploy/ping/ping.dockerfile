FROM golang:1.10

# Set env from build command: docker build --build-arg var=value ...
ARG DBNAME
ENV DBNAME=$DBNAME
ARG DBHOST
ENV DBHOST=$DBHOST
ARG DBUSER
ENV DBUSER=$DBUSER
ARG DBPASSWORD
ENV DBPASSWORD=$DBPASSWORD

# copy files & build application binary
WORKDIR /go/src/pingo
COPY . .
RUN cd /go/src/pingo/pingo-ping && go get .
RUN cd /go/src/pingo/pingo-ping && go install

EXPOSE 80

# Launch application when container starts
CMD pingo-ping