FROM golang:1.10

# Set env from build command: docker build --build-arg var=value ...
ARG DBNAME
ENV DBNAME=$DBNAME
ARG DBHOST
ENV DBHOST=$DBHOST
ARG DBUSER
ENV DBUSER=$DBUSER
ARG DBPASSWORD
ENV DBPASSWORD=$DBPASSWORD
ARG CORS_URLS
ENV CORS_URLS=$CORS_URLS

# API PORT can be set from env, but you will have to update port mapping in dockerrun.aws.json then
ARG API_PORT_ARG=3000
# EXPOSE PORT
EXPOSE ${API_PORT_ARG}
ENV API_PORT=$API_PORT_ARG

# copy files & build application binary
WORKDIR /go/src/pingo
COPY . .
RUN cd /go/src/pingo/pingo-api && go get .
RUN cd /go/src/pingo/pingo-api && go install

# Launch application when container starts
CMD pingo-api